package tourGuide.web;

import com.jsoniter.output.JsonStream;
import gpsUtil.location.VisitedLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tourGuide.domain.model.NearByAttractions;
import tourGuide.domain.model.User;
import tourGuide.domain.model.UserLocations;
import tourGuide.domain.model.UserPreferences;
import tourGuide.domain.service.TourGuideService;
import tripPricer.Provider;

import java.util.List;
import java.util.NoSuchElementException;

@Slf4j
@RestController
public class TourGuideController {

    private final TourGuideService tourGuideService;

    public TourGuideController(TourGuideService tourGuideService) {
        this.tourGuideService = tourGuideService;
    }

    @SuppressWarnings("SameReturnValue")
    @ApiOperation(value = "souhaite la bienvenue")
    @GetMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    @ApiOperation(value = "renvoie la localisation courante d'un utilisateur")
    @GetMapping("/getLocation")
    public String getLocation(@RequestParam String userName) {
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(tourGuideService.getUser(userName));
        return JsonStream.serialize(visitedLocation.location);
    }

    @ApiOperation(value = "renvoie les 5 attractions les plus proches d'un utilisateur")
    @GetMapping("/getNearbyAttractions")
    public NearByAttractions getNearbyAttractions(@RequestParam String userName) {
        return tourGuideService.getNearByAttractions(userName);
    }

    @ApiOperation(value = "renvoie les points de récompense d'un utilisateur")
    @GetMapping("/getRewards")
    public String getRewards(@RequestParam String userName) {
        return JsonStream.serialize(tourGuideService.getUserRewards(tourGuideService.getUser(userName)));
    }

    @ApiOperation(value = "renvoie toutes les précédentes localisations de tous les utilisateurs")
    @GetMapping("/getAllCurrentLocations")
    public List<UserLocations> getAllCurrentLocations() {
        return tourGuideService.getAllCurrentLocations();
    }


    @ApiOperation(value = "renvoie des offres de voyage pour un utilisateur")
    @GetMapping("/getTripDeals")
    public String getTripDeals(@RequestParam String userName) {
        List<Provider> providers = tourGuideService.getTripDeals(tourGuideService.getUser(userName));
        return JsonStream.serialize(providers);
    }

    @ApiOperation(value = "met à jour les préférences d'un utilisateur")
    @PutMapping("/userPreferences/{userName}")
    public ResponseEntity<?> putUserPreferences(@PathVariable String userName,
                                                @RequestBody UserPreferences userPreferences) {

        log.debug("request for set userPreferences of userName : {}", userName);

        try {
            User userSaved = tourGuideService.updateUserPreferences(userName, userPreferences);
            return ResponseEntity.status(HttpStatus.OK).body(userSaved);
        } catch (NoSuchElementException e) {
            String logAndBodyMessage = "error while putting user because missing user with userName=" + userName;
            log.error(logAndBodyMessage);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(logAndBodyMessage);
        }
    }
}